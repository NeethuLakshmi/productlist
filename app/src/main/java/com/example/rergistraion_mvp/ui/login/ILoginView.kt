package com.example.rergistraion_mvp.ui.login

import com.example.rergistraion_mvp.MvpView
import com.example.rergistraion_mvp.model.login.LoginResponse

interface ILoginView : MvpView {

    fun onSuccess(loginBase: LoginResponse)
    fun onError(error: Error) }