package com.example.rergistraion_mvp.ui.productlist

import com.example.rergistraion_mvp.model.productlist.ProductResponse

interface IProductView {
    fun onProductSuccess(loginBase: ProductResponse)
    fun onProductError(error: Error)
}