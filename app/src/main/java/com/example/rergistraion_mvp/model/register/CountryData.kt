package com.example.rergistraion_mvp.model.register

data class CountryData(

    val country_id: String,
    val country_name: String
)