package com.example.rergistraion_mvp.model.register

data class StateResponse ( val data: List<StateData>,
                           val message: String,
                           val status: String)