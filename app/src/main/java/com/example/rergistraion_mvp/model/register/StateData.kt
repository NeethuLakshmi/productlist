package com.example.rergistraion_mvp.model.register

data class StateData(val state_id: String,
                     val state_name: String)