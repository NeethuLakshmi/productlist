package com.example.rergistraion_mvp.util

import android.content.Context
import com.example.rergistraion_mvp.model.login.User

class SharedPrefrenceManager private constructor(private val mCtx: Context) {

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getInt("user_id", -1) != -1
        }

    val user: User
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return User(
                sharedPreferences.getInt("user_id", -1),
                sharedPreferences.getString("email_id", null),
                sharedPreferences.getInt("user_type", -1)
            )
        }


    fun saveUser(user: User) {

        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putInt("user_id", user.user_id)
        editor.putString("email_id", user.email_id)
        editor.putInt("user_type", user.user_type)

        editor.apply()

    }

    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private val SHARED_PREF_NAME = "my_shared_preff"
        private var mInstance: SharedPrefrenceManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPrefrenceManager {
            if (mInstance == null) {
                mInstance = SharedPrefrenceManager (mCtx)
            }
            return mInstance as SharedPrefrenceManager
        }
    }
}